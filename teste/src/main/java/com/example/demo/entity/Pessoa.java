package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//import io.swagger.annotations.ApiModelProperty;

@Entity
public class Pessoa
{
    //@ApiModelProperty(value = "Código da pessoa")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    //@ApiModelProperty(value = "Nome da pessoa")
    @Column(nullable = false)
    private String nome;
    
    //@ApiModelProperty(value = "CPF da pessoa")
    @Column
	private Long cpf;

	//@ApiModelProperty(value = "Data de Nascimento da pessoa")
    @Column
	private Date dataNascimento;
    
	//@ApiModelProperty(value = "idade da pessoa")
    @Column
    private int idade;
    
    public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
    
}